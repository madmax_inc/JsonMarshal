#ifndef JSONMARSHAL_ENTITYJSONMAPPERCONFIGURATOR_HPP
#define JSONMARSHAL_ENTITYJSONMAPPERCONFIGURATOR_HPP

#include "EntityJsonMapper.hpp"
#include "SimpleJsonMapper.hpp"

namespace JsonMarshal {

/**
 * @brief Configurator class used to break down the cyclic dependencies
 *
 * Internally uses the SimpleJsonMapper class
 * @see SimpleJsonMapper
 */
template <class T>
class EntityJsonMapperConfigurator {
private:
    EntityJsonMapper<T> *configurable;
    using Chainable = EntityJsonMapperConfigurator<T> &;

public:
    explicit EntityJsonMapperConfigurator(EntityJsonMapper<T> *configurable) : configurable(configurable) {}

    /**
     * @brief Method registers property with a type that supports default serialization (most primitive types, collection types, string types)
     * @tparam DataType Field data type
     * @param property JSON Property name
     * @param fieldAccessor FieldAccessor instance that provides access to the corresponding class field
     */
    template <class DataType>
    Chainable registerSerializer(const QString &property, const FieldAccessor<T, DataType> &fieldAccessor) {
        configurable->registerSerializer(property, std::unique_ptr<FieldSerializer<T>>{ new FieldSerializer<T> { new SimpleJsonMapper<DataType>(), fieldAccessor } });
        return *this;
    }

    /**
     * @brief Method registers property with a complex type (custom object) or a collection of such objects
     * @tparam CompositeDataType Field data type (class)
     * @param property JSON Property name
     * @param fieldAccessor FieldAccessor instance that provides access to the corresponding class field
     * @param compositeMapper EntityJsonMapper instance that could map JSON <-> CompositeDataType
     */
    template <class CompositeDataType, class EntityType>
    Chainable registerSerializer(const QString &property, const FieldAccessor<T, CompositeDataType> &fieldAccessor, const EntityJsonMapper<EntityType> *compositeMapper) {
        configurable->registerSerializer(property, std::unique_ptr<FieldSerializer<T>>{ new FieldSerializer<T> { new SimpleJsonMapper<CompositeDataType>(compositeMapper), fieldAccessor } });
        return *this;
    }
};

}

#endif //JSONMARSHAL_ENTITYJSONMAPPERCONFIGURATOR_HPP
