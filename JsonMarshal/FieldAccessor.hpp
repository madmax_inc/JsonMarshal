#ifndef JSONMARSHAL_FIELDACCESSOR_HPP
#define JSONMARSHAL_FIELDACCESSOR_HPP

#include <utility>

namespace JsonMarshal {

/**
 * @brief Normalizer/Denormalizer implementation
 *
 * Used to read/write a specific field to/from C++ object
 * Incapsulates read/wrtie functors (formerly std::mem_fn)
 *
 * @tparam Entity C++ object to manipulate on
 * @tparam DataType Property data type
 */
template <class Entity, class DataType>
class FieldAccessor {
public:
    using ReadMember = std::function<DataType(const Entity&)>;
    using WriteMember = std::function<void(Entity&, const DataType&)>;

private:
    ReadMember readMember;
    WriteMember writeMember;


public:
    FieldAccessor(ReadMember readMember, WriteMember writeMember) :
        readMember(readMember),
        writeMember(writeMember)
    {}

    DataType read(const Entity *obj) const { return read(*obj); }
    DataType read(const Entity &obj) const { return readMember(obj); }
    void write(Entity *obj, const DataType &data) const { write(*obj, data); }
    void write(Entity &obj, const DataType &data) const { writeMember(obj, data); }
};


/**
 * Convinience function that binds any r/w functors into a FieldAccessor instance
 */
template <class Entity, class DataType>
static FieldAccessor<Entity, DataType> make_accessor(
        typename FieldAccessor<Entity, DataType>::ReadMember readMember,
        typename FieldAccessor<Entity, DataType>::WriteMember writeMember
        ) {
    return FieldAccessor<Entity, DataType>(readMember, writeMember);
};


/**
 * Convinience function that binds member functions into a FieldAccessor instance
 */
template <class Entity, class DataType>
static FieldAccessor<Entity, DataType> make_accessor(
        DataType (Entity::*readMember)() const,
        void (Entity::*writeMember)(const DataType &)
        ) {
    return FieldAccessor<Entity, DataType>(readMember, writeMember);
};

/**
 * Convinience function that binds member functions into a FieldAccessor instance
 */
template <class Entity, class DataType>
static FieldAccessor<Entity, DataType> make_accessor(
        DataType (Entity::*readMember)() const,
        void (Entity::*writeMember)(DataType)
        ) {
    return FieldAccessor<Entity, DataType>(readMember, writeMember);
};

/**
 * Convenience function that binds member-available field (ex. public field) into a FieldAccessor instance
 */
template <class Entity, class DataType>
static FieldAccessor<Entity, DataType> make_accessor(
        DataType Entity::*availableField
        ) {
    return FieldAccessor<Entity, DataType>(
                [availableField] (const Entity &entity) -> DataType {
        return entity.*availableField;
    },
    [availableField] (Entity &entity, const DataType &data) {
        entity.*availableField = data;
    }
    );
};

}

#endif //JSONMARSHAL_FIELDACCESSOR_HPP
