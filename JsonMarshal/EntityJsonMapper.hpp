#ifndef JSONMARSHAL_ENTITYJSONMAPPER_HPP
#define JSONMARSHAL_ENTITYJSONMAPPER_HPP


#include <QtCore/QVector>

#include <QtCore/QJsonObject>
#include <QtCore/QJsonArray>
#include <QtCore/QString>
#include <QtCore/QHash>

#include <memory>
#include <unordered_map>

#include "FieldSerializer.hpp"

namespace std {

template <>
struct hash<QString> {
    std::size_t operator()(const QString &str) const {
        return static_cast<std::size_t>(::qHash(str));
    }
};

}

namespace JsonMarshal {

/**
 * @brief Class that enables JSON <-> Object serialization/deserialization
 * @tparam T The object type to be serialized/deserialized
 *
 * Serialization/Deserialization goes through 2 phases:
 * 1) Object-field access (called Normalization/Denormalization) allow data retrieval/delivery for a specified C++ data type (@see FieldSerializer)
 * 2) Data-Type <-> JSON mapping (called Serialization/Deserialization) allow transparent conversion between JSON data types and underlying C++ analogues (@see JsonMapper)
 *
 * These phases was implemented in FieldSerializer class (@see FieldSerializer)
 *
 * JSON field mappings must be registered in order to work
 * @see EntityJsonMapper::registerSerializer
 * @see EntityJsonMapperConfigurator::registerSerializer
 */
template <class T>
class EntityJsonMapper {
public:
    EntityJsonMapper() = default;
    EntityJsonMapper(const EntityJsonMapper<T>&) = delete;
    EntityJsonMapper &operator=(const EntityJsonMapper<T>&) = delete;
    EntityJsonMapper(EntityJsonMapper&&) noexcept = default;

    /**
     * Registers a property with custom serializer
     * Transfers ownership to the mapper
     *
     * @deprecated One should prefer the std::string/std::unique_ptr version
     * @param property JSON-Field name
     * @param serializer A serializer instance
     */
    void registerSerializer(const QString &property, FieldSerializer<T> *serializer) {
        registerSerializer(property, std::unique_ptr<FieldSerializer<T>>(serializer));
    }

    void registerSerializer(const QString &property, std::unique_ptr<FieldSerializer<T>> &&serializer) {
        serializers.insert(std::make_pair(property, std::move(serializer)));
    }

    /**
     * A convenience method to parse JSON-Array of objects
     *
     * @param array JSON-Array to parse
     * @return std::vector of C++ object instances
     */
    std::vector<T> parse(const QJsonArray &array) const {
        std::vector<T> result;
        result.resize(array.size());

        std::transform(array.constBegin(), array.constEnd(), result.begin(), [this] (const QJsonValue &value) -> T {
            return this->parse(value.toObject());
        });

        return result;
    }


    /**
     * Converts a JSON-Object into the C++ object instance
     *
     * @param object JSON-Object to parse
     * @return The C++ object instance
     */
    T parse(const QJsonObject &object) const {
        T entity;


        for (const auto &propertyPair : serializers) {
            const auto &property = propertyPair.first;
            serializers.at(property)->deserialize(object[property], entity);
        }

        return entity;
    }

    /**
     * Converts a C++ object instance into the JSON-Object (serialization)
     *
     * @param object The C++ object instance
     * @return JSON-Object
     */
    QJsonObject serialize(const T &object) const {
        QJsonObject obj;


        for (const auto &propertyPair : serializers) {
            const auto &property = propertyPair.first;
            obj[property] = serializers.at(property)->serialize(object);
        }

        return obj;
    }

private:
    std::unordered_map<QString, ::std::unique_ptr<FieldSerializer<T>>> serializers;

};

}

#endif //JSONMARSHAL_ENTITYJSONMAPPER_HPP
