#ifndef JSONMARSHAL_SIMPLEJSONMAPPER_HPP
#define JSONMARSHAL_SIMPLEJSONMAPPER_HPP

#include "EntityJsonMapper.hpp"
#include "JsonMapper.hpp"

#include <QJsonArray>

#include <QSharedPointer>
#include <memory>

namespace JsonMarshal {

/**
 * Type deduction helpers
 */
namespace {

/**
 * Meta-Type to distinguish pointer-like types
 *
 * The contract is:
 * 1) Static field "value" that is true if the type is a pointer-like type
 * 2) Typedef "ValueType" that is equal to a decayed object type
 * 3) Static methods "to" and "from" that enables conversion between reference and pointer-like type
 */
template <class Ptr>
struct IsPtr {
    /**
     * @brief value Compile-time flag to check whether the type is a pointer-type or not
     */
    static const bool value = false;
};

/**
 * Partial Specialization for raw pointers
 */
template <class T>
struct IsPtr<T*> {
    using ValueType = typename std::decay<T>::type;
    /**
     * @inheritdoc
     */
    static const bool value = true;

    static T *to(const ValueType &instance) {
        return new T(instance);
    }
    static T &from(T *ptr) {
        return *ptr;
    }
};

/**
 * Partial Specialization for std::shared_ptr
 */
template <class T>
struct IsPtr<std::shared_ptr<T>> {
    using ValueType = typename std::decay<T>::type;
    static const bool value = true;

    static std::shared_ptr<T> to(const ValueType &instance) {
        return std::make_shared<T>(instance);
    }
    static T &from(const std::shared_ptr<T> &ptr) {
        return *ptr;
    }
};

/**
 * Partial Specialization for QSharedPointer
 */
template <class T>
struct IsPtr<QSharedPointer<T>> {
    using ValueType = typename std::decay<T>::type;
    static const bool value = true;

    static QSharedPointer<T> to(const ValueType &instance) {
        return QSharedPointer<T>(new ValueType(instance));
    }
    static T &from(const std::shared_ptr<T> &ptr) {
        return *ptr;
    }
};

/**
 * Meta Type - std::decay analogue with support of pointer-like types
 * @example struct Foo {}; DecayHelper<const Foo&>::type; // => Foo
 * @example struct Bar {}; DecayHelper<std::shared_ptr<const Bar>>::type; // => Bar
 */
template <class T, class = void>
struct DecayHelper {
    using type = typename std::decay<T>::type;
};

template <class T>
struct DecayHelper<T, typename std::enable_if<IsPtr<T>::value>::type> {
    using type = typename IsPtr<T>::ValueType;
};


/**
  * Walter Brown's void_t based SFINAE
  * Compiles to void when all the arguments is well-formed
  * Effectively is:
  * template <class ...> using void_t = void;
  */

/**
 * Hack for GCC 4.9.2
 * Thanks: https://stackoverflow.com/questions/35753920/why-does-the-void-t-detection-idiom-not-work-with-gcc-4-9
 */
namespace void_details {
template <class... >
struct make_void { using type = void; };
}

template <class... T> using void_t = typename void_details ::make_void<T...>::type;

/**
 * Meta-Type to distinguish container types
 *
 * The decision is made based on std::begin and std::end specialization presence for a specific type
 * @see std::begin
 * @see std::end
 */
template <class T, class = void>
struct IsContainer {
    /**
     * @brief value Compile-time flag to check whether the type is a container type or not
     */
    static const bool value = false;
};

template <class T>
struct IsContainer<T, void_t<decltype(std::declval<const T&>().begin()), decltype(std::declval<const T&>().end())>> {
    /**
      * @inheritdoc
      */
    static const bool value = true;
    using PValueType = typename std::decay<decltype(*std::declval<const T&>().begin())>::type;
    using ValueType = typename DecayHelper<PValueType>::type;
};

/**
 * Meta-Type to distinguish string types
 */
template <class T>
struct IsString {
    static const bool value = false;
};

template <>
struct IsString<std::string> {
    static const bool value = true;

    static QJsonValue to(const std::string &str) { return QJsonValue(QString::fromStdString(str)); }
    static std::string from(const QJsonValue &val) { return val.toString().toStdString(); }
};

template <>
struct IsString<std::wstring> {
    static const bool value = true;

    static QJsonValue to(const std::wstring &str) { return QJsonValue(QString::fromStdWString(str)); }
    static std::wstring from(const QJsonValue &val) { return val.toString().toStdWString(); }
};

template <>
struct IsString<QString> {
    static const bool value = true;

    static QJsonValue to(const QString &str) { return QJsonValue(str); }
    static QString from(const QJsonValue &val) { return val.toString(); }
};

/**
 * Convinience meta-function to distinguish between containers and strings
 */
template <class T>
struct IsContainerNotString {
    static const bool value = IsContainer<T>::value && !IsString<T>::value;
};


/**
 * Convinience meta-function to distinguish between composite objects, pointer-like objects and containers
 */
template <class CompositeObject>
struct IsCompositeObject {
    static const bool value = !IsContainer<CompositeObject>::value && !IsPtr<CompositeObject>::value && std::is_class<CompositeObject>::value;
};

}

/**
 * SFINAE-based compile-time JsonMapper for a variety of types
 */
template <class DataType, class = void>
struct SimpleJsonMapper : JsonMapper<DataType> {};

/**
 * SFINAE Specialization for string types
 */
template <class StringType>
struct SimpleJsonMapper<StringType, typename std::enable_if<IsString<StringType>::value>::type> : JsonMapper<StringType> {
    virtual QJsonValue operator()(const StringType &data) const override {
        return IsString<StringType>::to(data);
    }

    virtual StringType operator()(const QJsonValue &value) const override {
        return IsString<StringType>::from(value);
    }
};

/**
 * SFINAE Specialization for integral types
 *
 * @todo Possibly need more attention on numeric limits here
 */
template <class IntegralType>
struct SimpleJsonMapper<IntegralType, typename std::enable_if<std::is_integral<IntegralType>::value>::type> : JsonMapper<IntegralType> {
    virtual QJsonValue operator()(const IntegralType &data) const override {
        return QJsonValue(static_cast<int>(data));
    }

    virtual IntegralType operator()(const QJsonValue &value) const override {
        return value.toInt();
    }
};

/**
 * SFINAE Specialization for floating types
 *
 * Internally converts JSON-argument to double
 */
template <class FloatingPointType>
struct SimpleJsonMapper<FloatingPointType, typename std::enable_if<std::is_floating_point<FloatingPointType>::value>::type> : JsonMapper<FloatingPointType> {
    virtual QJsonValue operator()(const FloatingPointType &data) const override {
        return QJsonValue(static_cast<double>(data));
    }

    virtual FloatingPointType operator()(const QJsonValue &value) const override {
        return value.toDouble();
    }
};

/**
 * SFINAE Specialization for a composite object
 *
 * Needs configured EntityJsonMapper in order to operate
 */
template <class CompositeObject>
class SimpleJsonMapper<CompositeObject, typename std::enable_if<IsCompositeObject<CompositeObject>::value>::type> : public JsonMapper<CompositeObject> {
private:
    const EntityJsonMapper<CompositeObject> *entityJsonMapper;

public:
    explicit SimpleJsonMapper(const EntityJsonMapper<CompositeObject> *entityJsonMapper) : entityJsonMapper(entityJsonMapper) {}

    virtual QJsonValue operator()(const CompositeObject &data) const override {
        return entityJsonMapper->serialize(data);
    }

    virtual CompositeObject operator()(const QJsonValue &value) const override {
        return entityJsonMapper->parse(value.toObject());
    }
};

/**
 * SFINAE Specialization for a pointer-like objects
 */
template <class Ptr>
class SimpleJsonMapper<Ptr, typename std::enable_if<IsPtr<Ptr>::value>::type> : public JsonMapper<Ptr> {
private:
    using Tester = IsPtr<Ptr>;
    using ValueType = typename Tester::ValueType;
    SimpleJsonMapper<ValueType> nestedMapper;

public:
    /**
     * Variadic-Templated constructor is in use in favor of different contained types (and corresponding SimpleJsonMappers)
     */
    template <class ... Args>
    SimpleJsonMapper(const Args &...args) : nestedMapper(args...) {}

    virtual QJsonValue operator()(const Ptr &data) const override {
        return nestedMapper(Tester::from(data));
    }

    virtual Ptr operator()(const QJsonValue &value) const override {
        return Tester::to(nestedMapper(value));
    }
};

/**
 * SFINAE Specialization for a containers
 */
template <class Container>
class SimpleJsonMapper<Container, typename std::enable_if<IsContainerNotString<Container>::value>::type> : public JsonMapper<Container> {
private:
    using ValueType = typename IsContainer<Container>::PValueType;
    SimpleJsonMapper<ValueType> nestedMapper;
public:
    /**
     * Variadic-Templated constructor is in use in favor of different contained types (and corresponding SimpleJsonMappers)
     */
    template <class ... Args>
    SimpleJsonMapper(const Args &...args) : nestedMapper(args...) {}

    virtual QJsonValue operator()(const Container &data) const override {
        QJsonArray array;

        for (auto it = std::begin(data); it != std::end(data); ++it) {
            array.append(nestedMapper(*it));
        }

        return array;
    }

    virtual Container operator()(const QJsonValue &value) const override {
        Container container;
        QJsonArray array = value.toArray();

        for (const QJsonValue &element : array) {
            *std::inserter(container, std::end(container)) = nestedMapper(element);
        }

        return container;
    }
};

}

#endif //JSONMARSHAL_SIMPLEJSONMAPPER_HPP
