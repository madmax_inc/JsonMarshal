#ifndef JSONMARSHAL_JSONMAPPER_HPP
#define JSONMARSHAL_JSONMAPPER_HPP

#include <QJsonValue>
#include <functional>

namespace JsonMarshal {

/**
 * Data Type Serializer/Deserializer interface
 * @tparam DataType A data type to convert to JSON and vice-versa
 */
template <class DataType>
class JsonMapper {
public:
    virtual ~JsonMapper() = default;
    virtual QJsonValue operator()(const DataType &data) const = 0;
    virtual DataType operator()(const QJsonValue &value) const = 0;
};

}


#endif //JSONMARSHAL_JSONMAPPER_HPP
