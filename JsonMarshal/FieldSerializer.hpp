#ifndef JSONMARSHAL_FIELDSERIALIZER_HPP
#define JSONMARSHAL_FIELDSERIALIZER_HPP

#include <QtCore/QJsonValue>
#include <functional>
#include <memory>
#include "JsonMapper.hpp"
#include "FieldAccessor.hpp"

namespace JsonMarshal {

/**
 * Normalizer/Denormalizer + Serializer/Deserializer bundle
 * Technically is a type-erasure usage to hide the Field-Name + DataType pair
 */
template<class T>
class FieldSerializer {
private:
    std::function<void(void)> finalizer;
    std::function<QJsonValue(const T&)> serializationImpl;
    std::function<void(QJsonValue, T&)> deserializationImpl;

public:
    template <class DataType>
    explicit FieldSerializer(
            JsonMapper<DataType> *mapper,
            const FieldAccessor <T, DataType> &accessor
    ) :
        finalizer([mapper] () { delete mapper; }),
        serializationImpl(
                [mapper, accessor] (const T &object) -> QJsonValue {
                    return (*mapper)(accessor.read(object));
                }),
        deserializationImpl(
                [mapper, accessor] (const QJsonValue &value, T &object) {
                    accessor.write(object, (*mapper)(value));
                })
    {}

    ~FieldSerializer() {
        finalizer();
    }

    QJsonValue serialize(const T &object) const {
        return serializationImpl(object);
    }

    void deserialize(const QJsonValue &value, T &object) const {
        deserializationImpl(value, object);
    }
};

}

#endif //JSONMARSHAL_FIELDSERIALIZER_H
