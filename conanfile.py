from conans import ConanFile


class JsonMarshalConan(ConanFile):
    name = "JsonMarshal"
    version = "1.3.1"
    license = "<Put the package license here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Jsonmarshal here>"
    exports_sources = "JsonMarshal/*"
    build_policy = "missing"

    def package(self):
        self.copy("*.h", dst="include/JsonMarshal", src="JsonMarshal")
        self.copy("*.hpp", dst="include/JsonMarshal", src="JsonMarshal")
