#include <JsonMarshal/EntityJsonMapper.hpp>
#include <JsonMarshal/EntityJsonMapperConfigurator.hpp>
#include <QtCore/QJsonDocument>
#include <iostream>

using namespace JsonMarshal;

struct Foo {
    std::string name;
    std::list<std::shared_ptr<Foo>> children;
};

void outDelim(std::ostream &stream, int count, char c = '\t') {
    while (count--) {
        stream << c;
    }
}

void outTo(const Foo &foo, std::ostream &stream, int amount = 0) {
    outDelim(stream, amount);
    stream << "{" << std::endl;
    outDelim(stream, amount + 1);
    stream << "name = " << foo.name << std::endl;
    outDelim(stream, amount + 1);
    stream << "children = [" << std::endl;

    for (auto fooPtr : foo.children) {
        outTo(*fooPtr, stream, amount + 1);
    }

    outDelim(stream, amount + 1);
    stream << "]" << std::endl;
    outDelim(stream, amount);
    stream << "}" << std::endl;
}


std::ostream &operator<<(std::ostream &stream, const Foo &foo) {
    outTo(foo, stream);
    return stream;
}


int main() {
    EntityJsonMapper<Foo> mapper;

    EntityJsonMapperConfigurator<Foo>(&mapper)
        .registerSerializer("name", make_accessor(&Foo::name))
        .registerSerializer("children", make_accessor(&Foo::children), &mapper);


    QString data = R"({ "name": "A", "children": [ { "name": "B", "children": [] }, { "name": "C", "children": [ { "name": "D", "children": [] } ] } ]})";

    Foo foo = mapper.parse(QJsonDocument::fromJson(data.toUtf8()).object());

    std::cout << foo << std::endl;

    std::cout << QJsonDocument(mapper.serialize(foo)).toJson().toStdString();
}