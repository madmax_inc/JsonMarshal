#include <iostream>
#include <set>
#include <QtCore/QJsonDocument>
#include <QDebug>
#include <JsonMarshal/EntityJsonMapperConfigurator.hpp>

using namespace JsonMarshal;

struct Bar {
    QString baz;

    bool operator<(const Bar &other) const {
        return baz < other.baz;
    }
};

class Foo {
public:
    double test;
    std::list<int> ints;

private:
    long id;
    QString name;
    Bar singleComposite;
    std::set<Bar> composite;

public:
    long getId() const {
        return id;
    }

    void setId(long id) {
        Foo::id = id;
    }

    QString getName() const {
        return name;
    }

    void setName(const QString &name) {
        Foo::name = name;
    }

    Bar getSingleComposite() const {
        return singleComposite;
    }

    void setSingleComposite(const Bar &singleComposite) {
        Foo::singleComposite = singleComposite;
    }

    std::set<Bar> getComposite() const {
        return composite;
    }

    void setComposite(const std::set<Bar> &composite) {
        Foo::composite = composite;
    }
};

int main() {
    EntityJsonMapper<Bar> barMapper;

    EntityJsonMapperConfigurator<Bar>(&barMapper)
            .registerSerializer("baz", make_accessor(&Bar::baz));


    EntityJsonMapper<Foo> mapper;

    EntityJsonMapperConfigurator<Foo>(&mapper)
            .registerSerializer("id", make_accessor(&Foo::getId, &Foo::setId))
            .registerSerializer("name", make_accessor(&Foo::getName, &Foo::setName))
            .registerSerializer("test", make_accessor(&Foo::test))
            .registerSerializer("list", make_accessor(&Foo::ints))
            .registerSerializer("composite", make_accessor(&Foo::getSingleComposite, &Foo::setSingleComposite), &barMapper)
            .registerSerializer("composites", make_accessor(&Foo::getComposite, &Foo::setComposite), &barMapper);

    QString sample = R"({ "id": 51, "name": "Hey!", "test": 3.5, "list": [ 1, 2, 3 ], "composite": { "baz": "QWERTY!" }, "composites": [ { "baz": "A" }, { "baz": "B" } ] })";
    Foo a = mapper.parse(QJsonDocument::fromJson(sample.toUtf8()).object());

    qDebug() << a.getId() << a.getName() << a.test << a.getSingleComposite().baz;

    for (const auto &i : a.ints) {
        qDebug() << i;
    }

    auto comp = a.getComposite();

    for (const auto &i : comp) {
        qDebug() << i.baz;
    }


    qDebug() << "\n" << mapper.serialize(a);

}
