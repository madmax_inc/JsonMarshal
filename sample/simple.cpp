#include <iostream>
#include <QString>
#include <JsonMarshal/EntityJsonMapper.hpp>
#include <JsonMarshal/EntityJsonMapperConfigurator.hpp>
#include <QtCore/QJsonDocument>


class Foo {
public:
    int a; // Any primitive type
    std::string b; //Or builtin std::string
    QString c;//Or a Qt analogue
    std::vector<std::string> d;//Container

private:
    double e;//Or something available through getters/setters

public:
    double getE() const {
        return e;
    }

    void setE(double e) {
        this->e = e;
    }
};


std::ostream &operator<<(std::ostream &stream, const Foo &obj) {
    stream << "a = " << obj.a << std::endl;
    stream << "b = " << obj.b << std::endl;
    stream << "c = " << obj.c.toStdString() << std::endl;
    stream << "d = { ";

    for (const std::string &str : obj.d) {
        stream << str << " ";
    }
    stream << "}" << std::endl;
    stream << "e = " << obj.getE() << std::endl;
}


int main() {
    JsonMarshal::EntityJsonMapper<Foo> mapper;

    JsonMarshal::EntityJsonMapperConfigurator<Foo>(&mapper)
        .registerSerializer("a", JsonMarshal::make_accessor(&Foo::a))
        .registerSerializer("b", JsonMarshal::make_accessor(&Foo::b))
        .registerSerializer("c", JsonMarshal::make_accessor(&Foo::c))
        .registerSerializer("d", JsonMarshal::make_accessor(&Foo::d))
        .registerSerializer("e", JsonMarshal::make_accessor(&Foo::getE, &Foo::setE));


    QString data = R"({ "a": 1, "b": "a string", "c": "A Qt-powered string", "d": [ "Hello", "World" ], "e": 3.5 })";

    QJsonObject jsonObject = QJsonDocument::fromJson(data.toUtf8()).object();

    Foo obj = mapper.parse(jsonObject);


    std::cout << obj;
}