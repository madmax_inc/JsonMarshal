# JsonMarshal
A simple library to convert data between JSON <-> Native C++ Objects.


[![pipeline status](https://gitlab.com/madmax_inc/JsonMarshal/badges/master/pipeline.svg)](https://gitlab.com/madmax_inc/JsonMarshal/commits/master)
[![coverage report](https://gitlab.com/madmax_inc/JsonMarshal/badges/master/coverage.svg)](https://gitlab.com/madmax_inc/JsonMarshal/commits/master)


## Features
* Blazing fast!
* Written entirely with template metaprogramming techniques. With SFINAE in heart!
* Clean and robust API
* Painless conversion between JSON and standard primitive types and containers


## Dependencies
1. QtCore - provides JSON support
2. C++11 complaint compiler

## Quick Start
Given an entity

```cpp
class Foo {
public:
    int a; // Any primitive type
    std::string b; //Or builtin std::string
    QString c;//Or a Qt analogue
    std::vector<std::string> d;//Container of primitive type

private:
    double e;//Or something available through getters/setters

public:
    double getE() const {
        return e;
    }

    void setE(double e) {
        this->e = e;
    }
};

std::ostream &operator<<(std::ostream &stream, const Foo &obj) {
    stream << "a = " << obj.a << std::endl;
    stream << "b = " << obj.b << std::endl;
    stream << "c = " << obj.c.toStdString() << std::endl;
    stream << "d = { ";

    for (const std::string &str : obj.d) {
        stream << str << " ";
    }
    stream << "}" << std::endl;
    stream << "e = " << obj.getE() << std::endl;
}
```

Create a JSON mapper object as follows
```cpp
JsonMarshal::EntityJsonMapper<Foo> mapper;
```

Configure field mappings directly (see docs for details) or via `EntityJsonMapperConfigurator` which provides nice builder-like API =)
```cpp
JsonMarshal::EntityJsonMapperConfigurator<Foo>(&mapper)
        .registerSerializer("a", JsonMarshal::make_accessor(&Foo::a))
        .registerSerializer("b", JsonMarshal::make_accessor(&Foo::b))
        .registerSerializer("c", JsonMarshal::make_accessor(&Foo::c))
        .registerSerializer("d", JsonMarshal::make_accessor(&Foo::d))
        .registerSerializer("e", JsonMarshal::make_accessor(&Foo::getE, &Foo::setE));
```

Use `EntityJsonMapper::parse` and `EntityJsonMapper::serialize` to convert data to and from JSON

```cpp
QString data = R"({ "a": 1, "b": "a string", "c": "A Qt-powered string", "d": [ "Hello", "World" ], "e": 3.5 })";
QJsonObject jsonObject = QJsonDocument::fromJson(data.toUtf8()).object();
    
Foo obj = mapper.parse(jsonObject);
        
std::cout << obj;
``` 


Enjoy =)
```bash
a = 1
b = a string
c = A Qt-powered string
d = { Hello World }
e = 3.5
```
