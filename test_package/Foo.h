#ifndef FOO_H
#define FOO_H

#include <QtGlobal>

struct Foo {
    int a;
    float b;
    int val;

    int get() const { return val; }
    void set(int a) { val = a; }

    bool operator==(const Foo &other) const { return a == other.a && qFuzzyCompare(b, other.b) && val == other.val; }
};

#endif
