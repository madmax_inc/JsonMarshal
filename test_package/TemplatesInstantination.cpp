#include "Foo.h"
#include <QString>
#include <QVector>
#include <memory>

#include "JsonMarshal/SimpleJsonMapper.hpp"
#include "JsonMarshal/EntityJsonMapper.hpp"
#include "JsonMarshal/EntityJsonMapperConfigurator.hpp"


/* SimpleJsonMapper Instantinations */
/* String types */
template class JsonMarshal::SimpleJsonMapper<std::string>;
template class JsonMarshal::SimpleJsonMapper<QString>;

/* Integral type */
template class JsonMarshal::SimpleJsonMapper<long>;

/* Floating-point type */
template class JsonMarshal::SimpleJsonMapper<float>;

/* Object type */
template class JsonMarshal::SimpleJsonMapper<Foo>;

/* Pointer-like types */
template class JsonMarshal::SimpleJsonMapper<Foo*>;
template class JsonMarshal::SimpleJsonMapper<std::shared_ptr<Foo>>;

/* Container type */
template class JsonMarshal::SimpleJsonMapper<QVector<int>>;

/* FieldSerializer */
template class JsonMarshal::FieldSerializer<Foo>;

/* FieldAccesors */
template class JsonMarshal::FieldAccessor<Foo, int>;
template static JsonMarshal::FieldAccessor<Foo, int> JsonMarshal::make_accessor(int (Foo::*)() const, void (Foo::*)(const int&));
template static JsonMarshal::FieldAccessor<Foo, int> JsonMarshal::make_accessor(int (Foo::*)() const, void (Foo::*)(int));
template static JsonMarshal::FieldAccessor<Foo, int> JsonMarshal::make_accessor(int Foo::*);


template class JsonMarshal::EntityJsonMapper<Foo>;
template class JsonMarshal::EntityJsonMapperConfigurator<Foo>;
