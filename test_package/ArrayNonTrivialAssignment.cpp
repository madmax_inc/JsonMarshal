#include <catch.hpp>
#include "JsonMarshal/JsonMapper.hpp"
#include "JsonMarshal/EntityJsonMapperConfigurator.hpp"
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>

namespace {

struct NonTrivial {
    int a;
    int b;
    std::string c;//Non-trivial assignment here
    int d;
};

}

TEST_CASE("Non-trivial assignment test") {
    const char *jsonObjs = R"([
  {
    "a": 1,
    "b": 2,
    "c": "ASDAS",
    "d": 3
  },
  {
    "a": 1,
    "b": 2,
    "c": "ASDAS",
    "d": 3
  }
])";

    JsonMarshal::EntityJsonMapper<NonTrivial> mapper;

    JsonMarshal::EntityJsonMapperConfigurator<NonTrivial> { &mapper }
        .registerSerializer("a", JsonMarshal::make_accessor(&NonTrivial::a))
        .registerSerializer("b", JsonMarshal::make_accessor(&NonTrivial::b))
        .registerSerializer("c", JsonMarshal::make_accessor(&NonTrivial::c))
        .registerSerializer("d", JsonMarshal::make_accessor(&NonTrivial::d));

    REQUIRE_NOTHROW(mapper.parse(QJsonDocument::fromJson(jsonObjs).array()));//Actually will be SIGABRT
}