#include <catch.hpp>
#include "JsonMarshal/FieldSerializer.hpp"
#include "JsonMarshal/SimpleJsonMapper.hpp"
#include "Foo.h"



class InvokeTracker {
public:
    void invoked() {
        count++;
    }

    std::size_t getCount() const {
        return count;
    }

private:
    std::size_t count = 0;
};


class TrackedMapper : public JsonMarshal::SimpleJsonMapper<int> {
public:
    TrackedMapper(InvokeTracker *tracker) :
        JsonMarshal::SimpleJsonMapper<int>(),
        m_tracker(tracker) {}

    ~TrackedMapper() {
        m_tracker->invoked();
    }

private:
    InvokeTracker *m_tracker;
};


TEST_CASE("Test the integration of JsonMapper & FieldAccessor via the FieldSerializer") {
    InvokeTracker tracker;
    {
    JsonMarshal::FieldSerializer<Foo> serializer(new TrackedMapper(&tracker), JsonMarshal::make_accessor(&Foo::a));
    REQUIRE(tracker.getCount() == 0);

    Foo instance { 42 };

    QJsonValue instanceVal { serializer.serialize(instance) };
    REQUIRE(instanceVal.isDouble());
    REQUIRE(instanceVal.toInt() == 42);


    serializer.deserialize(QJsonValue(128), instance);
    REQUIRE(instance.a == 128);
    }
    REQUIRE(tracker.getCount() == 1);//Verify that the lifetime is managed by serializer
}
