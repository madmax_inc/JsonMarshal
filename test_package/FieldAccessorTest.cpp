#include <catch.hpp>
#include "JsonMarshal/FieldAccessor.hpp"
#include "Foo.h"



TEST_CASE("Test getter/setter accessor generator") {
    auto accessor = JsonMarshal::make_accessor(&Foo::get, &Foo::set);

    Foo testable;
    const int starting = 0;
    testable.set(starting);

    REQUIRE(accessor.read(testable) == starting);
    accessor.write(testable, accessor.read(testable) + 1);
    REQUIRE(accessor.read(&testable) == (starting + 1));
}

TEST_CASE("Test struct member accessor generator") {
    auto accessor = JsonMarshal::make_accessor(&Foo::val);

    Foo a { .a = 0, .b = 0.0f, .val = 123 };

    REQUIRE(accessor.read(a) == 123);
    accessor.write(a, 456);
    REQUIRE(accessor.read(&a) == 456);
}
