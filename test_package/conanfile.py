from conans import ConanFile, CMake, tools
from six import StringIO
import os, re

class JsonMarshalTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    requires = "catch2/2.0.1@bincrafters/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def test(self):
        if not tools.cross_building(self.settings):
            coverage_enabled = True if tools.which("lcov") and tools.which("genhtml") else False

            if coverage_enabled:
                self.output.warn("Coverage utilities found")
                self.output.info("Capturing initial data...")
                self.run("lcov -c --initial --directory . --output-file test_initial.info")
            else:
                self.output.warn("Coverage utilities not found!")

            with tools.chdir("bin"):
                CMake(self).test()

            self.output.info("Capturing coverage data...")
            self.run("lcov -c --directory . --output-file test_coverage.info")

            self.output.info("Mixing coverage data...")
            self.run("lcov --add-tracefile test_initial.info --add-tracefile test_coverage.info --output-file test.info", output = None)

            self.output.info("Filtering coverage data...")
            self.run("lcov -e test.info *JsonMarshal/* --output-file test.info", output = None)
            self.run("lcov -r test.info *test_package/* --output-file test.info", output = None)

            self.output.info("Writing coverage report...")
            tools.mkdir("coverage")

            buf = StringIO()
            self.run("genhtml test.info --output-directory coverage", output = buf)
            pattern = re.compile("^\\s*lines\\.*:\\s([0-9\\.]+)%", re.MULTILINE)
            match = pattern.search(buf.getvalue())
            self.output.info("Coverage percentage: %s" % match.group(1))
