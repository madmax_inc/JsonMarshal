#include <catch.hpp>

#include "Foo.h"
#include "JsonMarshal/EntityJsonMapperConfigurator.hpp"
#include "JsonMarshal/SimpleJsonMapper.hpp"

namespace {

template <class StringType>
struct StringTester {
    static void test() {
        JsonMarshal::SimpleJsonMapper<StringType> mapper;

        const char *strVal = "qwerty";

        StringType a { mapper(QJsonValue(strVal)) };
        REQUIRE(a == strVal);
        QJsonValue jsonValue { mapper(a) };
        REQUIRE(jsonValue.isString());
        REQUIRE(jsonValue.toString() == strVal);
    }
};

template <class FooPointerLikeType>
struct PtrLikeTester {
    static void test() {
        static_assert(JsonMarshal::IsPtr<FooPointerLikeType>::value, "Must be a pointer-like type!");

        JsonMarshal::EntityJsonMapper<Foo> entityMapper;
        JsonMarshal::EntityJsonMapperConfigurator<Foo>(&entityMapper)
                .registerSerializer("a", JsonMarshal::make_accessor(&Foo::a))
                .registerSerializer("b", JsonMarshal::make_accessor(&Foo::b))
                .registerSerializer("val", JsonMarshal::make_accessor(&Foo::get, &Foo::set));

        JsonMarshal::SimpleJsonMapper<FooPointerLikeType> mapper(&entityMapper);

        Foo val { 42, 3.1415f, 0 };

        QJsonValue a { mapper(JsonMarshal::IsPtr<FooPointerLikeType>::to(val)) };

        REQUIRE(a.isObject());
        QJsonObject aObj { a.toObject() };
        REQUIRE(aObj["a"].toInt() == 42);
        REQUIRE(qFuzzyCompare(static_cast<float>(aObj["b"].toDouble()), 3.1415f));
        REQUIRE(*mapper(a) == val);
    }
};

}

TEST_CASE("Test SimpleJsonMapper for a string types") {
    StringTester<std::string>::test();
    StringTester<QString>::test();
}

TEST_CASE("Test SimpleJsonMapper for an integral type") {
    JsonMarshal::SimpleJsonMapper<long> mapper;

    const long val = 42;
    QJsonValue a { mapper(val) };

    REQUIRE(a.isDouble());
    REQUIRE(a.toInt() == val);
    REQUIRE(mapper(a) == val);
}

TEST_CASE("Test SimpleJsonMapper for a floating-point type") {
    JsonMarshal::SimpleJsonMapper<float> mapper;

    const float val = 3.1415f;
    QJsonValue a { mapper(val) };

    REQUIRE(a.isDouble());
    REQUIRE(qFuzzyCompare(static_cast<float>(a.toDouble()), val));
    REQUIRE(qFuzzyCompare(mapper(a), static_cast<float>(a.toDouble())));
}

TEST_CASE("Test SimpleJsonMapper for a composite object type") {
    JsonMarshal::EntityJsonMapper<Foo> entityMapper;
    JsonMarshal::EntityJsonMapperConfigurator<Foo>(&entityMapper)
            .registerSerializer("a", JsonMarshal::make_accessor(&Foo::a))
            .registerSerializer("b", JsonMarshal::make_accessor(&Foo::b))
            .registerSerializer("val", JsonMarshal::make_accessor(&Foo::get, &Foo::set));

    JsonMarshal::SimpleJsonMapper<Foo> mapper(&entityMapper);

    Foo val { 42, 3.1415f, 0 };

    QJsonValue a { mapper(val) };

    REQUIRE(a.isObject());
    QJsonObject aObj { a.toObject() };
    REQUIRE(aObj["a"].toInt() == 42);
    REQUIRE(qFuzzyCompare(static_cast<float>(aObj["b"].toDouble()), 3.1415f));
    REQUIRE(mapper(a) == val);
}


TEST_CASE("Test SimpleJsonMapper for a pointer-like reference") {
    PtrLikeTester<Foo*>::test();
    PtrLikeTester<std::shared_ptr<Foo>>::test();
}


TEST_CASE("Test SimpleJsonMapper for a container types") {
    JsonMarshal::SimpleJsonMapper<QVector<int>> mapper;

    const QVector<int> val { 1, 2, 3, 4, 5 };

    QJsonValue a { mapper(val) };

    REQUIRE(a.isArray());
    QJsonArray arr(a.toArray());

    REQUIRE(val.size() == arr.size());

    REQUIRE(val == mapper(a));
}
