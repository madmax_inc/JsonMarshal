#include <catch.hpp>
#include "JsonMarshal/EntityJsonMapper.hpp"
#include <type_traits>

TEST_CASE("Testing JsonMapper copying/movement capabilities") {
    using mapper = JsonMarshal::EntityJsonMapper<struct any>;

    REQUIRE_FALSE(std::is_copy_constructible<mapper>::value);
    REQUIRE_FALSE(std::is_copy_assignable<mapper>::value);
    REQUIRE(std::is_move_constructible<mapper>::value);
}